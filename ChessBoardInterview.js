/**
 * This is your takehome technical assessment.
 * Recommend using VSCode as an IDE, and NodeJS as an execution environment while testing your work.
 * 
 * You will be defining ES6 classes to represent a chess board and chess pieces,
 * then using those definitions to answer questions
 * 
 * The "answers" to the questions should be in the form of a function,
 * which when executed will perform the necessary instantiation and logic to arrive at the answer
 * 
 * You are NOT expected to create any kind of user interaction,
 * we should be able to execute your function and see results in the console upon its completion.
 * 
 

 /** 
 * Below are interface definitions for a Knight chess piece and a chess Board
 * implement both classes according to their definitions.
 */


/**
 * @class Knight
 * @description defines a Knight piece in chess
 * @property {number} x - the current x coordinate of the piece
 * @property {number} y - the current y coordinate of the piece 
 */
class Knight {
  /**
   * Constructs an instance of Knight piece with x and y coordinates
   * @param {Number} [x] - optional, defaults to 0
   * @param {Number} [y] - optional, defaults to 0
   */
  constructor(x=0,y=0) {

  }

  /**
   * deifnition for a coord, an x-y pair represented as an array of length 2
   * @typeDef {Number[]} coord - an array of length two
   * @property coord[0] - x coordinate
   * @property coord[1] - y coordinate
   */

  /**
   * all possible moves the instantiated Knight could take
   * @returns {coord[]} - an array of x,y coordinates representing the possible moves
   * @example a knight at x,y position 0,0 will return an array of x,y coordinates [1,2] [2,1] [-1,2] [-2,1] [2,-1] [1,-2] [-2,-1] [-1,-2]
   */
  availableMoves () {}
}

/**
 * @class Board
 * @description defines a chess game board
 * @property {Number[]} xValues - array of possible x coordinates, starts with 0
 * @property {Number[]} yValues - array of possible y coordinates, starts with 0
 * @property {Knight[]} pieces - array of pieces currently on the board
 */
class Board {

  /**
   * Constructs an instance of a Board
   * @param {Number} [xLength] - optional, the length of the x dimension of the board, defaults to 1, must be >= 1
   * @param {Number} [yLength] - optional, the length of the y dimension of the board, defaults to 1, must be >= 1
   * @param {Knight[]} [pieces] - optional, an array of pieces initially on the board, defaults to no pieces
   */
  constructor (xLength=1, yLength=1, pieces=[]){

  }
  
  /**
   * adds a piece to the game board
   * @param {Knight} piece - the piece to add to the board
   * @returns {Boolean} - true if the piece was successfully added, false if it was not added (for example, if it already exists on the board)
   */
  addPiece(piece) {

  }

  /**
   * removes a piece from the game board
   * @param {Knight} piece - the piece to remove from the board
   * @returns {Knight|false} - if the piece was removed, return the piece, otherwise return false if unable to remove the piece from the board (for example, if the piece is not currently on the board) 
   */
  removePiece(piece) {

  }

  /**
   * determines whether the given piece is allowed to make the requested move
   * @param {Knight} piece - the piece that will attempt to move
   * @param {Number} xCoord - the xCoordinate the piece wants to move to
   * @param {Number} yCoord - the yCoordinate the piece wants to move to
   * @returns {Boolean} - true if the requested move is valid for this board, false otherwise
   */
  validateMove(piece, xCoord, yCoord) {

  }

  /**
   * moves a piece on the board, and if the move resuts in taking a piece, removes the taken piece
   * @param {Knight} piece - the piece that will make a move
   * @param {Number} xCoord - the xCoordinate the piece is moving to
   * @param {Number} yCoord - the yCoordinate the piece is moving to
   * @returns {Knight|Boolean} - false if the piece failed to complete a move, true if the piece moved to an empty space, or the removed piece if the move results in taking a piece.
   */
  executeMove(piece, xCoord, yCoord){

  }

}
 

/**
 * QUESTION ONE
 * Create a 4x4 Board and 4 Knight Pieces
 * Place 3 of the Knight Pieces at x=0 y=1, x=2 y=2, and x=3 y=1
 * Place the 4th Knight at x=2 y=0
 * create a function that determines the fewest number of moves (less than 15) of the 4th Knight to capture the first 3 knights
 * capturing occurs when a piece makes a valid move to an already-occupied square
 * log to the console the fewest number of moves, and the x-y coordinates for each move the 4th Knight takes to perform the captures.
 */
function QuestionOne(){

}

/**
 * QUESTION TWO
 * Create a 4 x 4 Board and a Knight Piece
 * Place the Knight at coordinates x=2 y=0
 * create a function that determines a series of moves for the knight to touch every square on the board at least once
 * Log to the console the set of moves taken as an array of x-y coordiantes
 */
function QuestionTwo(){

}

/**
 * BONUS 1
 * Redefine the above classes such that there is a Piece class that the Knight class extends
 * Then, create another piece of your choice by extending the Piece class
 */

/**
 * BONUS 2
 * Enance the pieces to contain a new property 'color', which can be either white or black
 * Update the class definitions such that a piece cannot make a move that would result in capturing another piece of the same color.
 */

/** 
 * BONUS 3
 * Redo QUESTION ONE, where the knights at x=2 y=2 and x=2 y=0 are black,
 * the other two are white, and your 4th (black) night must capture the two white knights in the fewest number of moves
 */